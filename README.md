# Theme system prototype

## Redis
### [Install Redis in Mac OSX](http://lifesforlearning.com/install-redis-mac-osx/ "Install Redis in Mac OSX")

	curl -O http://download.redis.io/redis-stable.tar.gz
	tar -xvzf redis-stable.tar.gz 
	rm redis-stable.tar.gz 
	cd redis-stable 
	make 
	sudo make install

### Then to start Redis, you can enter this command.

	redis-server


## jsdoc

### [Best doc for node](http://samwize.com/2014/01/31/the-best-documentation-generator-for-node/)

	node ./node_modules/jsdoc/jsdoc.js proxyRouter.js -t ./node_modules/ink-docstrap/template -c ./node_modules/jsdoc/conf.json
	./out/index.html


## markdown-preview
### [NPM reference](https://www.npmjs.com/package/markdown-preview)

	npm install -g markdown-preview 
